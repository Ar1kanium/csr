export function useAuth() {
  const error = ref<string | null>(null)

  const login = async (name: string, password: string) => {
    try {
      const data: { token: string } = await $fetch('/api/login', {
        method: 'POST',
        body: { name, password },
      })

      setStorageData('authToken', data.token)
      await navigateTo('/admin')
    }
    catch (e) {
      error.value = 'Ошибка при авторизации'
    }
  }

  return { error, login }
}
