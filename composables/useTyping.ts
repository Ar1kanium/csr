export function useTyping(options: string[], typeSpeed = 100, waitTime = 3000) {
  const writing = ref('')
  let mode = 'typing'
  let counter = 0
  let intervalId: string | number | NodeJS.Timeout | undefined
  let timeoutId: string | number | NodeJS.Timeout | undefined
  onMounted(() => {
    intervalId = setInterval(() => {
      if (mode === 'typing') {
        if (writing.value.length < options[counter].length) {
          writing.value += options[counter][writing.value.length]
        }
        else {
          mode = 'idle'
          timeoutId = setTimeout(() => mode = 'deleting', waitTime)
        }
      }
      if (mode === 'deleting') {
        if (writing.value.length) {
          writing.value = writing.value.slice(0, writing.value.length - 1)
        }
        else {
          counter = counter === options.length - 1 ? 0 : counter + 1
          mode = 'typing'
        }
      }
    }, typeSpeed)
  })
  onUnmounted(() => {
    if (intervalId)
      clearInterval(intervalId)
    if (timeoutId)
      clearTimeout(timeoutId)
  })

  return writing
}
