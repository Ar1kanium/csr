const minIncrementForUnique = nums => {
    nums.sort((a,b) => a - b)
    let counter = 0
    for (let i = 1; i < nums.length; i++) {
        while (nums[i] <= nums[i - 1]) {
            nums[i]++
            counter++
        }
    }
    return counter
};