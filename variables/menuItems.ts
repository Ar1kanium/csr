import type { MenuItem } from "../interfaces/menuItem";

export const menuItems: MenuItem[] = [
    { page: '/', name: 'О компании' },
    { page: '/contact', name: 'Контакты' },
    { page: '/services', name: 'Услуги' },
    { page: '/careers', name: 'Вакансии' },
    { page: '/social-projects', name: 'Соцпроекты' },
]