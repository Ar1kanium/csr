import type { BaseEntity } from '../interfaces/baseEntity'

export function crudFactory<T extends BaseEntity>(resource: string, options = { state: {}, actions: {} }) {
  return defineStore(resource, {
    state: () => ({
      ...options?.state,
      [resource]: [] as T[],
    }),
    actions: {
      ...options?.actions,
      async getEntities(): Promise<void> {
        try {
          this[resource] = await $fetch(`/api/${resource}`)
        }
        catch (error) {
          console.error('load failed:', error)
        }
      },
      async createFileEntity(entity: T): Promise<void> {
        const formData = new FormData()
        Object.entries(entity).forEach(([key, value]) => formData.append(key, value))
        try {
          const response: T = await $fetch(`/api/${resource}`, {
            method: 'POST',
            body: formData,
          })
          this[resource] = [...this[resource], response]
          console.log('Uploaded:', response)
        }
        catch (error) {
          console.error('Upload failed:', error)
        }
      },
      async deleteEntity(id: number): Promise<void> {
        try {
          await $fetch(`/api/${resource}/${id}`, {
            method: 'DELETE',
          })
          this[resource] = this[resource].filter((entity: T) => entity.id !== id)
        }
        catch (error) {
          console.error('error', error)
        }
      },
    },
  })
}
