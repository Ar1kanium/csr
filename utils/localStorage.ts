export function getStorageData(key: string) {
  return localStorage.getItem(key)
}

export function setStorageData(key: string, data: any) {
  let currentValue = ''
  if (typeof data === 'string') {
    currentValue = data
  }
  else {
    try {
      currentValue = JSON.stringify(data)
    }
    catch (err) {
      console.warn(err)
    }
  }
  localStorage.setItem(key, currentValue)
}

export function clearStorage() {
  localStorage.clear()
}
