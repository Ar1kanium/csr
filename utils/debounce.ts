export function debounce(func: any, timeout: number | undefined) {
  let timeoutId: string | number | NodeJS.Timeout | undefined
  return (...args: any) => {
    clearTimeout(timeoutId)
    timeoutId = setTimeout(() => {
      func(...args)
    }, timeout)
  }
}
