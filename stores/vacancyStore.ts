import type { Vacancy } from '../interfaces/vacancy'
import { crudFactory } from '../utils/crudFactory'

export const useVacancyStore = crudFactory<Vacancy>('vacancy')
