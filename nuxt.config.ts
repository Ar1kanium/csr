export default defineNuxtConfig({
  modules: ['@pinia/nuxt', 'nuxt-svgo'],

  svgo: {
    componentPrefix: 'i',
    defaultImport: 'component',
  },

  devtools: { enabled: true },

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },

  app: {
    head: {
      link: [{ rel: 'icon', type: 'image/png', href: '/favicon.ico' }],
    },
  },

  css: ['@/assets/css/tailwind.css'],
  compatibilityDate: '2024-07-02',
})
