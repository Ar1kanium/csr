import jwt from 'jsonwebtoken'

export default defineEventHandler(async (event) => {
  try {
    if (event.node.req.method !== 'GET' && (event.path.startsWith('/api') && event.path !== '/api/login')) {
      const token = event.node.req.headers.authorization?.split(' ')[1]
      const SECRET_KEY = process.env.SECRET_KEY

      if (!SECRET_KEY) {
        throw new Error('JWT_SECRET is not defined')
      }

      if (!token) {
        throw new Error('token is not defined')
      }
      const decoded = jwt.verify(token, SECRET_KEY)
      event.context.user = decoded
    }
  }
  catch (err) {
    throw createError({ statusCode: 401, statusText: 'token denied' })
  }
},
)
