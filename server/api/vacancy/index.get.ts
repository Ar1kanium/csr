import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export default defineEventHandler(async (event) => {
  const vacancies = await prisma.vacancy.findMany()
  return [...vacancies]
})
