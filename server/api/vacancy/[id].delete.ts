import path from 'node:path'
import fs from 'node:fs'
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export default defineEventHandler(async (event) => {
  try {
    const id = Number.parseInt(event.context.params.id) as number
    const vacancy = await prisma.vacancy.findUnique({ where: { id } })
    const imagePath = path.join(process.cwd(), 'public', vacancy?.imageSrc as string)
    fs.unlinkSync(imagePath)
    await prisma.vacancy.delete({ where: { id } })
    return id
  }
  catch (error) {
    throw createError({ statusCode: 400, message: 'ошибка' })
  }
})
