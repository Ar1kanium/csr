import path from 'node:path'
import fs from 'node:fs'
import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()

export default defineEventHandler(async (event) => {
  const data = await readMultipartFormData(event)
  const image = data?.find(el => el.name === 'file')
  const title = data?.find(el => el.name === 'title')?.data.toString()
  const description = data?.find(el => el.name === 'description')?.data.toString()
  if (!image || !description || !title) {
    throw createError({ statusCode: 400, message: 'не все прислали' })
  }
  const relativePath = '/images/vacancies'
  const imagePath = path.join(process.cwd(), 'public', relativePath, image?.filename as string)
  fs.writeFileSync(imagePath, image.data)

  const vacancy = await prisma.vacancy.create({
    data: {
      title,
      description,
      imageSrc: `${relativePath}/${image?.filename as string}`,
    },
  })

  return vacancy
})
