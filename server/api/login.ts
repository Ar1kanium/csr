import { PrismaClient } from '@prisma/client'
import jwt from 'jsonwebtoken'

const prisma = new PrismaClient()

export default defineEventHandler(async (event) => {
  const body = await readBody(event)
  const user = await prisma.user.findUnique({
    where: {
      name: body.name,
    },
  })
  const SECRET_KEY = process.env.SECRET_KEY

  if (!SECRET_KEY) {
    throw new Error('JWT_SECRET is not defined')
  }

  if (!user || body?.password !== user.password) {
    throw createError({ statusCode: 401, statusMessage: 'Unauthorized' })
  }

  const token = jwt.sign({ id: user.id, name: user.name }, SECRET_KEY, { expiresIn: '24h' })

  return { token }
})
