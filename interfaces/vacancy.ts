import type { BaseEntity } from './baseEntity'

export interface Vacancy extends BaseEntity {
  title: string
  description: string
  imageSrc?: string
  file?: Blob
}
