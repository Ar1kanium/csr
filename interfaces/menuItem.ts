export interface MenuItem {
    page: string,
    name: string,
}