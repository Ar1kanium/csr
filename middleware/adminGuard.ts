export default defineNuxtRouteMiddleware((to) => {
  if (import.meta.client && !getStorageData('authToken'))
    return navigateTo('/login')
})
