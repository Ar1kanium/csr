import { ofetch } from 'ofetch'

export default defineNuxtPlugin((_nuxtApp) => {
  globalThis.$fetch = ofetch.create({
    onRequest({ options }) {
      const token = getStorageData('authToken')
      if (token) {
        options.headers = { Authorization: `Bearer ${token}` }
      }
    },
    async onResponseError({ response }) {
      console.log(response)
      if (response.status === 401)
        await navigateTo({ path: '/login' })
    },
  },
  )
})
