/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './components/**/*.{vue,js}',
    './layouts/**/*.{vue,js}',
    './pages/**/*.{vue,js}',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    './app.vue',
  ],
  theme: {
    extend: {
      colors: {
        customBg: '#1E252B',
        customGold: '#FF9E00',
        customFont: '#CBD5E1',
        customFont2: '#64748B',
        customSection: '#283036',
        borderDivider: '#343E48',
        /*            customBg: '#0E1524', */
      },
    },
    fontFamily: {
      Nunito: ['Nunito', 'ui-sans-serif'],
    },
  },
  plugins: [],
}
